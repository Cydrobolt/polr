polr
====

###This branch is for Polr PaaS. Are you looking for the self-hosted project? https://github.com/Cydrobolt/polr/tree/master

#####Would like to contribute? Take a look at the Todos, get familiar with our code, and submit a pull request. We'll see what we can do.


###Blockers (* need * to be done):

- create ratelimiting system (quota for api)
- allow settings to be changed
- port PaaS to Self-Hosted

####Todos:

- Update Self-hosted option w/ PaaS code
- **Create a logo for Polr**
- OOP-ize the shorten functions
- Improve UCP
- Better statistics
- More links options
- Code cleaning
- **Optimize display in mobile browsers**
- Other ideas? Give us a pull req.

###Please do not submit pull requests to this repo. This is simply a BitBucket clone of the main Github Repo, located at https://github.com/Cydrobolt/polr.

http://polr.cf - Main Site
